# Conductor Introduction
![Conductor](https://i.imgur.com/1tcIEwJ.png)

## Apps
- [conductor-this-then-that](https://gitlab.com/conductor/apps/conductor-this-then-that)
- [conductor-api-ai](https://gitlab.com/conductor/apps/conductor-api-ai-app)
- [conductor-telegram](https://gitlab.com/conductor/apps/conductor-telegram-app)

## Router
- [conductor-router](https://gitlab.com/conductor/core/conductor-router)

## Administration
- [conductor-administration](https://gitlab.com/conductor/administration/conductor-administration)

## Repository
- [conductor-repository](https://gitlab.com/conductor/repositories/conductor-repository)

## Control Unit
- [conductor-control-unit-java](https://gitlab.com/conductor/control-units/conductor-control-unit-java)

## Component Groups
- [spotify](https://gitlab.com/conductor/component-groups/spotify-component-group)
- [youtube](https://gitlab.com/conductor/component-groups/youtube-component-group)
- [hypem](https://gitlab.com/conductor/component-groups/hypem-component-group)
- [windows](https://gitlab.com/conductor/component-groups/windows-component-group)
- [volvo-s80](https://gitlab.com/conductor/component-groups/volvo-s80-component-group)
- [vlc](https://gitlab.com/conductor/component-groups/vlc-component-group)
- [philips ptf5500](https://gitlab.com/conductor/component-groups/philips-pft5500-component-group)